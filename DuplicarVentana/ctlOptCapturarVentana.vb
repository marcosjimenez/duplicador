﻿Imports System.Runtime.InteropServices

Public Class ctlOptCapturarVentana

    Declare Function GetWindowDC Lib "user32.dll" (ByVal hwnd As IntPtr) As IntPtr
    Declare Function ReleaseDC Lib "user32.dll" (ByVal hwnd As IntPtr, ByVal hdc As IntPtr) As Int32
    Declare Function GetWindowRect Lib "user32.dll" (ByVal hwnd As IntPtr, ByRef lpRect As RECT) As Int32
    Declare Function CreateCompatibleDC Lib "gdi32.dll" (ByVal hdc As IntPtr) As IntPtr
    Declare Function CreateCompatibleBitmap Lib "gdi32.dll" (ByVal hdc As IntPtr, ByVal nWidth As Int32, ByVal nHeight As Int32) As IntPtr
    Declare Function SelectObject Lib "gdi32.dll" (ByVal hdc As IntPtr, ByVal hObject As IntPtr) As IntPtr
    Declare Function BitBlt Lib "gdi32.dll" (ByVal hDestDC As IntPtr, ByVal x As Int32, ByVal y As Int32, ByVal nWidth As Int32, ByVal nHeight As Int32, ByVal hSrcDC As IntPtr, ByVal xSrc As Int32, ByVal ySrc As Int32, ByVal dwRop As Int32) As Int32
    Declare Function DeleteDC Lib "gdi32.dll" (ByVal hdc As IntPtr) As Int32
    Declare Function DeleteObject Lib "gdi32.dll" (ByVal hObject As IntPtr) As Int32

    <StructLayout(LayoutKind.Sequential)> _
    Public Structure RECT
        Public left As Integer
        Public top As Integer
        Public right As Integer
        Public bottom As Integer
    End Structure 'RECT

    Public Event Seleccion(ByVal hWnd As Long)

    Public Function CaptureWindow(ByVal handle As IntPtr) As Image
        Dim SRCCOPY As Integer = &HCC0020
        ' get te hDC of the target window
        Dim hdcSrc As IntPtr = GetWindowDC(handle)
        ' get the size
        Dim windowRect As New RECT
        GetWindowRect(handle, windowRect)
        Dim width As Integer = windowRect.right - windowRect.left
        Dim height As Integer = windowRect.bottom - windowRect.top
        ' create a device context we can copy to
        Dim hdcDest As IntPtr = CreateCompatibleDC(hdcSrc)
        ' create a bitmap we can copy it to,
        ' using GetDeviceCaps to get the width/height
        Dim hBitmap As IntPtr = CreateCompatibleBitmap(hdcSrc, width, height)
        ' select the bitmap object
        Dim hOld As IntPtr = SelectObject(hdcDest, hBitmap)
        ' bitblt over
        BitBlt(hdcDest, 0, 0, width, height, hdcSrc, 0, 0, SRCCOPY)
        ' restore selection
        SelectObject(hdcDest, hOld)
        ' clean up 
        DeleteDC(hdcDest)
        ReleaseDC(handle, hdcSrc)

        ' get a .NET image object for it
        Dim img As Image = Image.FromHbitmap(hBitmap)
        ' free up the Bitmap object
        DeleteObject(hBitmap)

        Return img
    End Function 'CaptureWindow

    Public Sub Listar()

        Dim itm As ListViewItem
        For Each x As Process In System.Diagnostics.Process.GetProcesses()

            If x.MainWindowTitle.Trim.Length > 0 Then
                itm = ListView1.Items.Add(x.MainWindowTitle)
                itm.SubItems.Add(x.MainWindowHandle.ToString)
            End If
        Next

    End Sub

    Private Sub ListView1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.SelectedIndexChanged

        If ListView1.SelectedItems.Count > 0 Then

            Try
                Dim HwND As Long = Convert.ToInt64(ListView1.SelectedItems(0).SubItems(1).Text)
                Dim mImg As Image = CaptureWindow(New IntPtr(HwND))
                PictureBox1.SizeMode = PictureBoxSizeMode.AutoSize

                RaiseEvent Seleccion(HwND)

            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

        Else
            RaiseEvent Seleccion(0)
        End If

    End Sub

End Class
