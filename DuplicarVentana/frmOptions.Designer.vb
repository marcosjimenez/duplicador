<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOptions
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOptions))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboScreen = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtAncho = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtFont = New System.Windows.Forms.TextBox()
        Me.cmdFont = New System.Windows.Forms.Button()
        Me.txtIntervalo = New System.Windows.Forms.NumericUpDown()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtFPS = New System.Windows.Forms.NumericUpDown()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.optCuadrado = New System.Windows.Forms.RadioButton()
        Me.optCirculo = New System.Windows.Forms.RadioButton()
        Me.cmdColorclick = New System.Windows.Forms.Button()
        Me.chkDibujarCursor = New System.Windows.Forms.CheckBox()
        Me.chkSeñalarClick = New System.Windows.Forms.CheckBox()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.picColorclick = New System.Windows.Forms.PictureBox()
        Me.CtlOptCapturarVentana1 = New DuplicarPantalla.ctlOptCapturarVentana()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.txtIntervalo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtFPS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picColorclick, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(289, 257)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(194, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(91, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "Aceptar"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Cancel_Button.Location = New System.Drawing.Point(100, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(91, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancelar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Pantalla a capturar:"
        '
        'cboScreen
        '
        Me.cboScreen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScreen.FormattingEnabled = True
        Me.cboScreen.Location = New System.Drawing.Point(6, 29)
        Me.cboScreen.Name = "cboScreen"
        Me.cboScreen.Size = New System.Drawing.Size(156, 21)
        Me.cboScreen.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(165, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Resolución:"
        '
        'txtAncho
        '
        Me.txtAncho.Location = New System.Drawing.Point(168, 30)
        Me.txtAncho.Name = "txtAncho"
        Me.txtAncho.ReadOnly = True
        Me.txtAncho.Size = New System.Drawing.Size(199, 20)
        Me.txtAncho.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(204, 138)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(114, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Fuente para mensajes:"
        '
        'txtFont
        '
        Me.txtFont.Location = New System.Drawing.Point(207, 154)
        Me.txtFont.Name = "txtFont"
        Me.txtFont.ReadOnly = True
        Me.txtFont.Size = New System.Drawing.Size(206, 20)
        Me.txtFont.TabIndex = 8
        Me.txtFont.Text = "Texto de Prueba"
        '
        'cmdFont
        '
        Me.cmdFont.Location = New System.Drawing.Point(419, 154)
        Me.cmdFont.Name = "cmdFont"
        Me.cmdFont.Size = New System.Drawing.Size(29, 20)
        Me.cmdFont.TabIndex = 9
        Me.cmdFont.Text = "..."
        Me.cmdFont.UseVisualStyleBackColor = True
        '
        'txtIntervalo
        '
        Me.txtIntervalo.Location = New System.Drawing.Point(197, 23)
        Me.txtIntervalo.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.txtIntervalo.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtIntervalo.Name = "txtIntervalo"
        Me.txtIntervalo.Size = New System.Drawing.Size(53, 20)
        Me.txtIntervalo.TabIndex = 10
        Me.txtIntervalo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIntervalo.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 25)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(185, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Intervalo actualización (milisegundos):"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtFPS)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtIntervalo)
        Me.GroupBox1.Location = New System.Drawing.Point(204, 17)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(260, 90)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Intervalos"
        '
        'txtFPS
        '
        Me.txtFPS.Location = New System.Drawing.Point(197, 61)
        Me.txtFPS.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.txtFPS.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtFPS.Name = "txtFPS"
        Me.txtFPS.Size = New System.Drawing.Size(53, 20)
        Me.txtFPS.TabIndex = 13
        Me.txtFPS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFPS.Value = New Decimal(New Integer() {25, 0, 0, 0})
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(65, 63)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(126, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "FPS Grabación de video:"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(10, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(478, 240)
        Me.TabControl1.TabIndex = 15
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox1)
        Me.TabPage3.Controls.Add(Me.PictureBox1)
        Me.TabPage3.Controls.Add(Me.Label5)
        Me.TabPage3.Controls.Add(Me.cmdFont)
        Me.TabPage3.Controls.Add(Me.txtFont)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(470, 214)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "General"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.PictureBox2)
        Me.TabPage4.Controls.Add(Me.GroupBox2)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(470, 214)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Cursor"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.optCuadrado)
        Me.GroupBox2.Controls.Add(Me.optCirculo)
        Me.GroupBox2.Controls.Add(Me.cmdColorclick)
        Me.GroupBox2.Controls.Add(Me.picColorclick)
        Me.GroupBox2.Controls.Add(Me.chkDibujarCursor)
        Me.GroupBox2.Controls.Add(Me.chkSeñalarClick)
        Me.GroupBox2.Location = New System.Drawing.Point(204, 21)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(260, 152)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cursor"
        '
        'optCuadrado
        '
        Me.optCuadrado.AutoSize = True
        Me.optCuadrado.Enabled = False
        Me.optCuadrado.Location = New System.Drawing.Point(24, 88)
        Me.optCuadrado.Name = "optCuadrado"
        Me.optCuadrado.Size = New System.Drawing.Size(107, 17)
        Me.optCuadrado.TabIndex = 19
        Me.optCuadrado.Text = "Con un cuadrado"
        Me.optCuadrado.UseVisualStyleBackColor = True
        '
        'optCirculo
        '
        Me.optCirculo.AutoSize = True
        Me.optCirculo.Checked = True
        Me.optCirculo.Enabled = False
        Me.optCirculo.Location = New System.Drawing.Point(24, 65)
        Me.optCirculo.Name = "optCirculo"
        Me.optCirculo.Size = New System.Drawing.Size(95, 17)
        Me.optCirculo.TabIndex = 18
        Me.optCirculo.TabStop = True
        Me.optCirculo.Text = "Con un círculo"
        Me.optCirculo.UseVisualStyleBackColor = True
        '
        'cmdColorclick
        '
        Me.cmdColorclick.Enabled = False
        Me.cmdColorclick.Location = New System.Drawing.Point(62, 114)
        Me.cmdColorclick.Name = "cmdColorclick"
        Me.cmdColorclick.Size = New System.Drawing.Size(113, 32)
        Me.cmdColorclick.TabIndex = 17
        Me.cmdColorclick.Text = "Seleccionar Color"
        Me.cmdColorclick.UseVisualStyleBackColor = True
        '
        'chkDibujarCursor
        '
        Me.chkDibujarCursor.AutoSize = True
        Me.chkDibujarCursor.Checked = True
        Me.chkDibujarCursor.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDibujarCursor.Location = New System.Drawing.Point(6, 19)
        Me.chkDibujarCursor.Name = "chkDibujarCursor"
        Me.chkDibujarCursor.Size = New System.Drawing.Size(91, 17)
        Me.chkDibujarCursor.TabIndex = 14
        Me.chkDibujarCursor.Text = "Dibujar cursor"
        Me.chkDibujarCursor.UseVisualStyleBackColor = True
        '
        'chkSeñalarClick
        '
        Me.chkSeñalarClick.AutoSize = True
        Me.chkSeñalarClick.Enabled = False
        Me.chkSeñalarClick.Location = New System.Drawing.Point(6, 42)
        Me.chkSeñalarClick.Name = "chkSeñalarClick"
        Me.chkSeñalarClick.Size = New System.Drawing.Size(93, 17)
        Me.chkSeñalarClick.TabIndex = 15
        Me.chkSeñalarClick.Text = "Señalar Clicks"
        Me.chkSeñalarClick.UseVisualStyleBackColor = True
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.cboScreen)
        Me.TabPage1.Controls.Add(Me.txtAncho)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(470, 214)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Capturar Pantalla"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.CtlOptCapturarVentana1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(470, 214)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Capturar Ventana"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.DuplicarPantalla.My.Resources.Resources._316
        Me.PictureBox1.Location = New System.Drawing.Point(6, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(192, 192)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 12
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(6, 6)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(192, 192)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 18
        Me.PictureBox2.TabStop = False
        '
        'picColorclick
        '
        Me.picColorclick.BackColor = System.Drawing.SystemColors.Info
        Me.picColorclick.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.picColorclick.Location = New System.Drawing.Point(24, 114)
        Me.picColorclick.Name = "picColorclick"
        Me.picColorclick.Size = New System.Drawing.Size(32, 32)
        Me.picColorclick.TabIndex = 16
        Me.picColorclick.TabStop = False
        '
        'CtlOptCapturarVentana1
        '
        Me.CtlOptCapturarVentana1.Location = New System.Drawing.Point(6, 6)
        Me.CtlOptCapturarVentana1.Name = "CtlOptCapturarVentana1"
        Me.CtlOptCapturarVentana1.Size = New System.Drawing.Size(458, 202)
        Me.CtlOptCapturarVentana1.TabIndex = 0
        '
        'frmOptions
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(495, 298)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOptions"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Opciones"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.txtIntervalo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtFPS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picColorclick, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboScreen As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtAncho As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtFont As System.Windows.Forms.TextBox
    Friend WithEvents cmdFont As System.Windows.Forms.Button
    Friend WithEvents txtIntervalo As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFPS As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdColorclick As System.Windows.Forms.Button
    Friend WithEvents picColorclick As System.Windows.Forms.PictureBox
    Friend WithEvents chkDibujarCursor As System.Windows.Forms.CheckBox
    Friend WithEvents chkSeñalarClick As System.Windows.Forms.CheckBox
    Friend WithEvents optCuadrado As System.Windows.Forms.RadioButton
    Friend WithEvents optCirculo As System.Windows.Forms.RadioButton
    Friend WithEvents CtlOptCapturarVentana1 As DuplicarPantalla.ctlOptCapturarVentana

End Class
