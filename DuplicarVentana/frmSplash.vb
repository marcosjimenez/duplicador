﻿Public Class frmSplash

    Private Sub frmSplash_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim img As System.Drawing.Bitmap = New System.Drawing.Bitmap(My.Resources.Logo)
        'Me.Size = img.Size
        img.MakeTransparent(img.GetPixel(img.Size.Width - 1, img.Size.Height - 1))
        Me.BackgroundImage = img
        Me.TransparencyKey = img.GetPixel(img.Size.Width - 1, img.Size.Height - 1)
        Me.AllowTransparency = True
    End Sub
End Class