Public Class frmVentanas

    Private m_Hwnd As Long
    Public ReadOnly Property hWnd As Long
        Get
            Return m_Hwnd
        End Get
    End Property

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub frmVentanas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CtlOptCapturarVentana1.Listar()

    End Sub

    Private Sub CtlOptCapturarVentana1_Seleccion(ByVal hWnd As Long) Handles CtlOptCapturarVentana1.Seleccion

        cmdAceptar.Enabled = (hWnd <> 0)
        m_Hwnd = hWnd
    End Sub
End Class