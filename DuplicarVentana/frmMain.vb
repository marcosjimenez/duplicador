Imports System.Runtime.InteropServices

Public Class frmMain

    Declare Function GetWindowDC Lib "user32.dll" (ByVal hwnd As IntPtr) As IntPtr
    Declare Function ReleaseDC Lib "user32.dll" (ByVal hwnd As IntPtr, ByVal hdc As IntPtr) As Int32
    Declare Function GetWindowRect Lib "user32.dll" (ByVal hwnd As IntPtr, ByRef lpRect As RECT) As Int32
    Declare Function CreateCompatibleDC Lib "gdi32.dll" (ByVal hdc As IntPtr) As IntPtr
    Declare Function CreateCompatibleBitmap Lib "gdi32.dll" (ByVal hdc As IntPtr, ByVal nWidth As Int32, ByVal nHeight As Int32) As IntPtr
    Declare Function SelectObject Lib "gdi32.dll" (ByVal hdc As IntPtr, ByVal hObject As IntPtr) As IntPtr
    Declare Function BitBlt Lib "gdi32.dll" (ByVal hDestDC As IntPtr, ByVal x As Int32, ByVal y As Int32, ByVal nWidth As Int32, ByVal nHeight As Int32, ByVal hSrcDC As IntPtr, ByVal xSrc As Int32, ByVal ySrc As Int32, ByVal dwRop As Int32) As Int32
    Declare Function DeleteDC Lib "gdi32.dll" (ByVal hdc As IntPtr) As Int32
    Declare Function DeleteObject Lib "gdi32.dll" (ByVal hObject As IntPtr) As Int32

    <StructLayout(LayoutKind.Sequential)> _
    Public Structure RECT
        Public left As Integer
        Public top As Integer
        Public right As Integer
        Public bottom As Integer
    End Structure 'RECT

    Private mScreenFont As Font
    Private bMouseIn As Boolean = False
    Private mPantalla As Integer = 0
    Private bRecording As Boolean = False
    Private WithEvents ControlesForm As frmControles

    Private avi As New AviWriter
    Private aviBmp As Bitmap

    Private nCount As Integer = 0 'Contador im�genes grabadas

    Public Enum enTipoCaptura
        CapturarPantalla = 0
        CapturarVentana = 1
    End Enum
    Private mTipoCaptura As enTipoCaptura = enTipoCaptura.CapturarVentana
    Private hWndCaptura As IntPtr

    Private Sub ShowOptions()

        Using frm As New frmOptions
            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                My.Settings.PantallaSeleccionada = frm.cboScreen.SelectedIndex
                My.Settings.ScreenFont = frm.txtFont.Font
                My.Settings.Intervalo = frm.txtIntervalo.Value
                My.Settings.IntervaloVideo = frm.txtFPS.Value
                My.Settings.DibujarCursor = frm.chkDibujarCursor.Checked
                Me.SetOptions()
            End If
        End Using

    End Sub

    Private Sub SetOptions()
        Timer1.Interval = My.Settings.Intervalo
        mPantalla = My.Settings.PantallaSeleccionada
        Me.SetFontSize()
    End Sub

    Private Sub SetFontSize()
        mScreenFont = New Font(My.Settings.ScreenFont.SystemFontName, Convert.ToSingle(Screen.AllScreens(mPantalla).Bounds.Height / 25), GraphicsUnit.Pixel)
    End Sub

    Private Sub CopyScreen()
        Dim gr As Graphics = PictureBox1.CreateGraphics

        Dim fSize As Size = Screen.AllScreens(0).Bounds.Size
        Dim bm As Bitmap = New Bitmap(fSize.Width, fSize.Height, gr)

        Dim gr2 As Graphics = Graphics.FromImage(bm)

        'Status
        gr2.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
        gr2.TextRenderingHint = Drawing.Text.TextRenderingHint.AntiAlias
        gr2.CompositingQuality = Drawing2D.CompositingQuality.HighSpeed
        gr2.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBilinear

        'Imagen
        gr2.CopyFromScreen(Screen.AllScreens(mPantalla).WorkingArea.X, Screen.AllScreens(mPantalla).WorkingArea.Y, 0, 0, fSize)

        'Cursor
        If My.Settings.DibujarCursor Then Cursor.Draw(gr2, New Rectangle(Cursor.Position, Cursor.Size))

        If bRecording Then

            gr2.DrawString("Grabando...", mScreenFont, Brushes.White, 0, 0)
            'gr2.DrawString("H: " + fSize.Height.ToString + " W: " + fSize.Width.ToString, mScreenFont, Brushes.White, 0, 0)
            'Dim nh As Single = 16
            'Dim nw As Single = 16
            'gr2.ScaleTransform(nh, nw, Drawing2D.MatrixOrder.Prepend)

            'gr2.DrawImage(My.Resources.RecordHS, 0, 0, nh, nw)

            Dim grAvi As Graphics = Graphics.FromImage(aviBmp)
            grAvi.CopyFromScreen(Screen.AllScreens(mPantalla).WorkingArea.X, Screen.AllScreens(mPantalla).WorkingArea.Y, 0, 0, fSize)
            avi.AddFrame()
            nCount += 1
            Me.Text = "Duplicador " + nCount.ToString
        End If

        Me.PictureBox1.Image = bm

    End Sub

    Public Sub CaptureWindow(ByVal handle As IntPtr)

        If handle.ToInt32 = 0 Then Exit Sub

        Dim SRCCOPY As Integer = &HCC0020
        ' get te hDC of the target window
        Dim hdcSrc As IntPtr = GetWindowDC(handle)
        ' get the size
        Dim windowRect As New RECT
        GetWindowRect(handle, windowRect)
        Dim width As Integer = windowRect.right - windowRect.left
        Dim height As Integer = windowRect.bottom - windowRect.top
        ' create a device context we can copy to
        Dim hdcDest As IntPtr = CreateCompatibleDC(hdcSrc)
        ' create a bitmap we can copy it to,
        ' using GetDeviceCaps to get the width/height
        Dim hBitmap As IntPtr = CreateCompatibleBitmap(hdcSrc, width, height)
        ' select the bitmap object
        Dim hOld As IntPtr = SelectObject(hdcDest, hBitmap)

        ' bitblt over
        BitBlt(hdcDest, 0, 0, width, height, hdcSrc, 0, 0, SRCCOPY)
        ' restore selection
        SelectObject(hdcDest, hOld)
        ' clean up 
        DeleteDC(hdcDest)
        ReleaseDC(handle, hdcSrc)

        ' get a .NET image object for it
        Dim img As Image = Image.FromHbitmap(hBitmap)
        ' free up the Bitmap object
        DeleteObject(hBitmap)

        PictureBox1.Image = img
    End Sub 'CaptureWindow


    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Timer1.Enabled = False
        If bRecording Then
            avi.Close()
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.SetOptions()
        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If mTipoCaptura = enTipoCaptura.CapturarPantalla Then
            Me.CopyScreen()
        Else
            If hWndCaptura.ToInt32 = 0 Then
                Timer1.Enabled = False
                Using frm As New frmVentanas
                    If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                        hWndCaptura = New IntPtr(frm.hWnd)
                    End If
                End Using
                Timer1.Enabled = True
            Else
                Me.CaptureWindow(hWndCaptura)
            End If

        End If
    End Sub

    Private Sub OpcionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpcionesToolStripMenuItem.Click
        Me.ShowOptions()
    End Sub

    Private Sub PictureBox1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox1.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then ContextMenuStrip1.Show(PictureBox1, e.Location)
    End Sub

    Private Sub PictureBox1_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox1.MouseLeave
        bMouseIn = False
    End Sub

    Private Sub PictureBox1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox1.MouseMove
        bMouseIn = (e.Y < (PictureBox1.Height / 4))
    End Sub

    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub ControlesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ControlesToolStripMenuItem.Click

        If ControlesToolStripMenuItem.Checked Then
            ControlesForm.Focus()
        Else
            ControlesForm = New frmControles
            ControlesForm.Show(Me)
            ControlesToolStripMenuItem.Checked = True
        End If

    End Sub

    Private Sub ControlesForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles ControlesForm.FormClosing
        ControlesToolStripMenuItem.Checked = False
    End Sub

    Private Sub ControlesForm_Grabar(ByVal bGrabar As Boolean, ByRef bCancel As Boolean) Handles ControlesForm.Grabar

        Using dlg As New SaveFileDialog()
            dlg.Filter = "*.avi|*.avi"
            If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                aviBmp = avi.Open("c:\Avi.avi", My.Settings.IntervaloVideo, PictureBox1.Width, PictureBox1.Height)
                bRecording = bGrabar
            Else
                bCancel = True
            End If
        End Using

    End Sub

    Private Sub ControlesForm_Opciones() Handles ControlesForm.Opciones
        Me.ShowOptions()
    End Sub

    Private Sub ControlesForm_PararGrabacion() Handles ControlesForm.PararGrabacion

        bRecording = False
        nCount = 0
        avi.Close()
        aviBmp = Nothing
        avi = New AviWriter
    End Sub

    Private Sub PictureBox1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox1.Resize
        Me.SetFontSize()
    End Sub

    Private Sub AcercaDeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AcercaDeToolStripMenuItem.Click
        Using frm As New frmAbout
            frm.ShowDialog()
        End Using
    End Sub
End Class
