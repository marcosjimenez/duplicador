Imports System.Windows.Forms

Public Class frmOptions

    Private Sub DatosPantalla()

        txtAncho.Text = Screen.AllScreens(cboScreen.SelectedIndex).Bounds.Width.ToString + " x " + Screen.AllScreens(cboScreen.SelectedIndex).Bounds.Height.ToString + " " + Screen.AllScreens(cboScreen.SelectedIndex).BitsPerPixel.ToString + "b"

    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub frmOptions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Pantalla
        For i As Integer = 0 To Screen.AllScreens.Length - 1
            cboScreen.Items.Add("Pantalla " + (i + 1).ToString)
        Next
        cboScreen.SelectedIndex = My.Settings.PantallaSeleccionada

        Me.DatosPantalla()

        'Fuente
        txtFont.Font = My.Settings.ScreenFont
        txtFont.Text = My.Settings.ScreenFont.Name

        'Intervalos
        txtIntervalo.Value = My.Settings.Intervalo
        txtFPS.Value = My.Settings.IntervaloVideo

        'Cursor
        chkDibujarCursor.Checked = My.Settings.DibujarCursor

        'Ventanas
        CtlOptCapturarVentana1.Listar()

    End Sub

    Private Sub cboScreen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboScreen.SelectedIndexChanged
        Me.DatosPantalla()
    End Sub

    Private Sub cmdFont_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFont.Click
        Using dlg As New FontDialog
            dlg.Font = txtFont.Font
            If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFont.Font = dlg.Font
                txtFont.Text = dlg.Font.Name
            End If
        End Using
    End Sub

    Private Sub txtIntervalo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtIntervalo.ValueChanged

    End Sub

    Private Sub txtFPS_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFPS.ValueChanged

    End Sub

    Private Sub chkSe�alarClick_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSe�alarClick.CheckedChanged
        Dim bEnable As Boolean = chkSe�alarClick.Checked
        optCirculo.Enabled = benable
        optCuadrado.Enabled = bEnable
        picColorclick.Enabled = bEnable
        cmdColorclick.Enabled = bEnable
    End Sub

    Private Sub optCirculo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optCirculo.CheckedChanged

    End Sub

    Private Sub cmdColorclick_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdColorclick.Click

        Using dlg As New ColorDialog
            dlg.Color = picColorclick.BackColor
            If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then picColorclick.BackColor = dlg.Color
        End Using

    End Sub
End Class
