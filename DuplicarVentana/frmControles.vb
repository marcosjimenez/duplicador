Public Class frmControles

    Public Event Grabar(ByVal bGrabar As Boolean, ByRef bCancel As Boolean)
    Public Event PararGrabacion()
    Public Event GuardarVideo()
    Public Event Opciones()

    Private Sub cmdGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGrabar.Click
        Dim bCancel As Boolean = False
        RaiseEvent Grabar(True, bCancel)
        If Not bCancel Then
            cmdGrabar.Enabled = False
            cmdStop.Enabled = True
        End If
    End Sub

    Private Sub cmdOpciones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOpciones.Click
        RaiseEvent Opciones()
    End Sub

    Private Sub cmdStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdStop.Click
        RaiseEvent PararGrabacion()
        cmdGrabar.Enabled = True
        cmdStop.Enabled = False
    End Sub

    Private Sub cmdGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGuardar.Click
        RaiseEvent GuardarVideo()
    End Sub
End Class